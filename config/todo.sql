--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: tasks; Type: TABLE; Schema: public; Owner: lauratrehout
--

CREATE TABLE public.tasks (
    id bigint NOT NULL,
    task_title character varying(200) NOT NULL,
    userid integer,
    completed boolean,
    task_date date,
    task_description text
);


ALTER TABLE public.tasks OWNER TO lauratrehout;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: lauratrehout
--

CREATE SEQUENCE public.tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO lauratrehout;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lauratrehout
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: lauratrehout
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    user_name character varying(100) NOT NULL,
    user_email character varying(100),
    hashedpassword character varying(200)
);


ALTER TABLE public.users OWNER TO lauratrehout;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: lauratrehout
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO lauratrehout;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lauratrehout
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: lauratrehout
--

COPY public.tasks (id, task_title, userid, completed, task_date, task_description) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: lauratrehout
--

COPY public.users (id, user_name, user_email, hashedpassword) FROM stdin;
\.


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lauratrehout
--

SELECT pg_catalog.setval('public.tasks_id_seq', 54, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lauratrehout
--

SELECT pg_catalog.setval('public.users_id_seq', 49, true);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_user_email_key; Type: CONSTRAINT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_email_key UNIQUE (user_email);


--
-- Name: tasks fk_task_user; Type: FK CONSTRAINT; Schema: public; Owner: lauratrehout
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT fk_task_user FOREIGN KEY (userid) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

