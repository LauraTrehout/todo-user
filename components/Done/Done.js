import { useSelector } from "react-redux";

import DoneList from "./DoneList";

import {
  DoneContainer,
  DoneTaskTitleContainer,
} from "../../styles/Container.styled";

function Done() {
  const tasks = useSelector((state) => state.tasks.tasks);
  const doneTasks = tasks.filter((task) => task.completed === true);

  return (
    <>
      <DoneTaskTitleContainer>
        <p>Tâches terminées</p>
        {doneTasks.length}
      </DoneTaskTitleContainer>
      <DoneContainer>
        <DoneList />
      </DoneContainer>
    </>
  );
}

export default Done;
