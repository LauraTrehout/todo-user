import { useDispatch, useSelector } from "react-redux";

import fr from "date-fns/locale/fr";
import format from "date-fns/format";

import {
  newSelectedTask,
  resetSelectedTask,
} from "../../redux/actions/selectedTask.actions";
import { setCompleted } from "../../redux/actions/tasks.actions";

import { Check } from "@styled-icons/bootstrap/Check";
import { DoneCheckbox } from "../../styles/Checkbox.styled";
import {
  DoneItemContainer,
  FlexContainer,
  TaskDetails,
} from "../../styles/Container.styled";
import {
  CrossedOutDate,
  CrossedOutTask,
  CrossedOutUser,
} from "../../styles/Title.styled";

const DoneItem = ({ task }) => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);

  const handleTaskClick = () => {
    dispatch(newSelectedTask(task));
  };
  const toggleTask = async () => {
    dispatch(
      setCompleted({
        userid: selectedTask.userid,
        completed: false,
        task_date: selectedTask.task_date,
        task_description: selectedTask.task_description,
        task_id: selectedTask.id,
      })
    );
    dispatch(resetSelectedTask());
  };

  const getDate = () => {
    if (task.task_date) {
      if (
        task.task_date.toString().slice(0, 10) ===
        new Date().toString().slice(0, 10)
      ) {
        return "Aujourd'hui";
      } else if (task.task_date === "") {
        return "";
      } else {
        return format(new Date(task.task_date), "dd MMMM yyyy", { locale: fr });
      }
    }
  };

  const getUser = () => {
    if (task.userid !== "") {
      return users.map((user) => {
        if (task.userid == user.id) {
          return user.user_name;
        }
      });
    } else {
      return "";
    }
  };
  return (
    <DoneItemContainer>
      <FlexContainer>
        <DoneCheckbox onClick={toggleTask}>
          <Check color="white" size="20px" />
        </DoneCheckbox>
        <TaskDetails>
          <CrossedOutTask onClick={handleTaskClick}>
            {task.task_title}
          </CrossedOutTask>
          <CrossedOutUser>{getUser()}</CrossedOutUser>
        </TaskDetails>
      </FlexContainer>
      <CrossedOutDate>{getDate()}</CrossedOutDate>
    </DoneItemContainer>
  );
};

export default DoneItem;
