import { useSelector } from "react-redux";

import DoneItem from "./DoneItem";

import { DoneListContainer } from "../../styles/Container.styled";

const DoneList = () => {
  const tasks = useSelector((state) => state.tasks.tasks);

  return (
    <DoneListContainer>
      {tasks &&
        tasks
          .filter((task) => task.completed === true)
          .map((task, index) => (
            <DoneItem
              key={index}
              task={task}
            />
          ))}
    </DoneListContainer>
  );
};

export default DoneList;
