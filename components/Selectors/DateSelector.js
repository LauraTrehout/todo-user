import { useDispatch, useSelector } from "react-redux";

import DatePicker from "react-datepicker";
import fr from "date-fns/locale/fr";
import format from "date-fns/format";

import { updateDate } from "../../redux/actions/tasks.actions";

import { TimeFive } from "@styled-icons/boxicons-regular";
import "react-datepicker/dist/react-datepicker.css";
import { AddDate } from "../../styles/Button.styled";
import {
  DateContainer,
  DateSelectorDiv,
  SelectorFlex,
} from "../../styles/Selector.styled";
import { DatepickerInput } from "../../styles/Input.styled";

const DateSelector = ({ toggleTaskDate, showDate, setShowDate }) => {
  const dispatch = useDispatch();
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);

  const handleDateSelect = (date) => {
    dispatch(
      updateDate({
        userid: selectedTask.userid,
        completed: false,
        task_date: date,
        task_description: selectedTask.task_description,
        task_id: selectedTask.id,
      })
    );
    setShowDate(false);
  };

  const getDate = () => {
    if (!selectedTask.task_date) {
      return "ÈCHÈANCE";
    } else {
      if (
        selectedTask.task_date.toString().slice(0, 10) ===
        new Date().toString().slice(0, 10)
      ) {
        return "Aujourd'hui";
      } else if (selectedTask.task_date === "") {
        return "";
      } else {
        return format(new Date(selectedTask.task_date), "dd/MM/yy", { locale: fr });
      }
    }
  };

  return (
    <>
      <DateSelectorDiv>
        <SelectorFlex>
          <AddDate onClick={toggleTaskDate}>
            <TimeFive color="grey" size="20" />
          </AddDate>
          {getDate()}
        </SelectorFlex>
      </DateSelectorDiv>
      {showDate && (
        <DateContainer>
          <DatePicker
            onChange={(date) => {
              handleDateSelect(date);
            }}
            locale={fr}
            dateFormat="d MMMM yyyy"
            minDate={new Date()}
            customInput={<DatepickerInput />}
          />
        </DateContainer>
      )}
    </>
  );
};

export default DateSelector;
