import UserSelector from "./UserSelector";
import DateSelector from "./DateSelector";

import { FlexContainer } from "../../styles/Container.styled";
import { useState } from "react";

const Selectors = () => {
  const [showUsers, setShowUsers] = useState(false);
  const [showDate, setShowDate] = useState(false);
  
  const toggleUserClick = () => {
    setShowUsers(!showUsers);
  };
  const toggleTaskDate = () => {
    setShowDate(!showDate);
  };

  return (
    <FlexContainer>
      <UserSelector
        showUsers={showUsers}
        setShowUsers={setShowUsers}
        toggleUserClick={toggleUserClick}
      />
      <DateSelector
        toggleTaskDate={toggleTaskDate}
        showDate={showDate}
        setShowDate={setShowDate}
      />
    </FlexContainer>
  );
};

export default Selectors;
