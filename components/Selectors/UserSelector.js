import { useSelector } from "react-redux";

import User from "../User";

import { AddUser } from "../../styles/Button.styled";
import {
  SelectorFlex,
  SelectUser,
  UserSelectorDiv,
} from "../../styles/Selector.styled";

const UserSelector = ({ showUsers, setShowUsers, toggleUserClick }) => {
  const users = useSelector((state) => state.users.users);
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);

  const getUser = () => {
    if (selectedTask.userid) {
      return users.map((user) => {
        if (selectedTask.userid == user.id) {
          return user.user_name;
        }
      });
    } else {
      return "ATTRIBUER À";
    }
  };

  return (
    <>
      <UserSelectorDiv>
        <SelectorFlex>
          <AddUser onClick={toggleUserClick}>+</AddUser>
          <p>{getUser()}</p>
        </SelectorFlex>
      </UserSelectorDiv>
      {showUsers && (
        <SelectUser>
          {users.map((user) => (
            <User
              key={user.id}
              user={user}
              showUsers={showUsers}
              setShowUsers={setShowUsers}
            />
          ))}
        </SelectUser>
      )}
    </>
  );
};

export default UserSelector;
