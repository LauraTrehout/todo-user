import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { login, getUsers, setUsers } from "../../redux/actions/users.actions";

import { AuthButton } from "../../styles/Button.styled";
import { AuthContainer } from "../../styles/Container.styled";
import { AuthForm, AuthFormLabel } from "../../styles/Form.styled";
import { AuthInput } from "../../styles/Input.styled";
import { AuthTitle } from "../../styles/Title.styled";

const Login = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(login({ email: email, password: password }));
  };

  return (
    <AuthContainer>
      <AuthTitle>Login</AuthTitle>
      <AuthForm>
        <AuthFormLabel htmlFor="login-email">Email</AuthFormLabel>
        <AuthInput
          type="email"
          id="login-email"
          onChange={handleEmailChange}
          required
        />
        <AuthFormLabel htmlFor="login-password">Password</AuthFormLabel>
        <AuthInput
          type="password"
          id="login-password"
          onChange={handlePasswordChange}
          required
        />
      </AuthForm>
      <AuthButton type="submit" onClick={(e) => handleSubmit(e)}>
        Login
      </AuthButton>
    </AuthContainer>
  );
};

export default Login;
