import { useDispatch } from "react-redux";

import { logout } from "../../redux/actions/users.actions";

import { LogoutButton } from "../../styles/Button.styled";

const Logout = () => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    localStorage.setItem("access_token", "");
    dispatch(logout());
  };
  return (
    <>
      <LogoutButton type="submit" onClick={(e) => handleLogout(e)}>
        Logout
      </LogoutButton>
    </>
  );
};

export default Logout;
