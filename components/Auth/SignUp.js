import { useState } from "react";
import { useDispatch } from 'react-redux'

import { AuthButton } from "../../styles/Button.styled";
import { AuthContainer } from "../../styles/Container.styled";
import { AuthForm, AuthFormLabel } from "../../styles/Form.styled";
import { AuthInput } from "../../styles/Input.styled";
import { AuthTitle } from "../../styles/Title.styled";
import { signup } from "../../redux/actions/users.actions";

const SignUp = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch()
  const handleNameChange = (e) => {
    setName(e.target.value);
  };
  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch(signup({ name: name, email: email, password: password }))

  };
  return (
    <AuthContainer>
      <AuthTitle>Sign Up</AuthTitle>
      <AuthForm>
        <AuthFormLabel htmlFor="name">Name</AuthFormLabel>
        <AuthInput
          type="text"
          id="name"
          onChange={handleNameChange}
          required
        />
        <AuthFormLabel htmlFor="signup-email">Email</AuthFormLabel>
        <AuthInput
          type="email"
          id="signup-email"
          onChange={handleEmailChange}
          required
        />
        <AuthFormLabel htmlFor="signup-password">Password</AuthFormLabel>
        <AuthInput
          type="password"
          id="signup-password"
          onChange={handlePasswordChange}
          required
        />
      </AuthForm>
      <AuthButton onClick={(event) => handleSubmit(event)}>Sign Up</AuthButton>
    </AuthContainer>
  );
};

export default SignUp;
