import { useSelector, useDispatch } from "react-redux";

import TodoList from "./TodoList";

import { TodoContainer } from "../../styles/Container.styled";
import { AllTasksTitle } from "../../styles/Title.styled";
import { Input } from "../../styles/Input.styled";

import jwt_decode from "jwt-decode";
import { newTask } from "../../redux/actions/tasks.actions";

function Todo({ input, setInput }) {
  const dispatch = useDispatch()
  const token = useSelector((state) => state.users.token);
  const token_id = jwt_decode(token).id;

  const handleInputChange = (e) => {
    setInput(e.target.value);
  };

  const handleAddTodo = async (e) => {
    if (input !== "") {
      dispatch(
        newTask({
          task_title: input,
          completed: false,
          userid: token_id,
        })
      );
      setInput("");
    }
  };

  return (
    <TodoContainer>
      <AllTasksTitle>Toutes les tâches</AllTasksTitle>
      <Input
        type="text"
        placeholder=" + Ajouter une tâche"
        value={input}
        onChange={handleInputChange}
        onKeyDown={(e) => e.key === "Enter" && handleAddTodo()}
      />
      <TodoList input={input} />
    </TodoContainer>
  );
}

export default Todo;

