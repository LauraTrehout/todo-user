import { useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { getTasks } from "../../redux/actions/tasks.actions";

import jwt_decode from "jwt-decode";

import TodoItem from "./TodoItem";

import { TodoListContainer } from "../../styles/Container.styled";

const TodoList = ({ input }) => {
  const dispatch = useDispatch();
  const tasks = useSelector((state) => state.tasks.tasks);
  const token = useSelector((state) => state.users.token);
  const token_id = jwt_decode(token).id;
  useEffect(() => {
    axios.post(`${process.env.NEXT_PUBLIC_SERVER}/api/auth/verifyToken`, {
      token: token,
    });
  }, [token]);

  useEffect(() => {
    dispatch(getTasks(token_id));
  }, [input, dispatch, token_id, tasks]);

  return (
    <TodoListContainer>
      {tasks
        .filter((task) => task.completed === false)
        .map((task) => (
          <TodoItem key={task.id} task={task} token_id={token_id} />
        ))}
    </TodoListContainer>
  );
};

export default TodoList;
