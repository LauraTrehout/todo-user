import { useSelector, useDispatch } from "react-redux";
import { useEffect } from 'react'
import Selectors from "./Selectors/Selectors";

import { Check } from "@styled-icons/bootstrap/Check";
import { PencilSquare } from "@styled-icons/bootstrap/PencilSquare";
import { Todo } from "@styled-icons/remix-fill/Todo";
import {
  CommentSection,
  FlexContainer,
  SelectedTaskContainer,
  TaskDescription,
} from "../styles/Container.styled";
import { CheckedButton, CommentButton } from "../styles/Button.styled";
import { Description, SelectedTaskTitle } from "../styles/Title.styled";
import { SelectedHeader } from "../styles/Header.styled";
import { TextArea } from "../styles/Input.styled";
import { useState } from "react";
import {
  updateDescription,
  setDescription,
} from "../redux/actions/tasks.actions";

function SelectedTask() {
  const dispatch = useDispatch();
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);
  const [taskDescription, setTaskDescription] = useState("");

  const handleTaskChange = (e) => {
    setTaskDescription(e.target.value);
  };

  const handleDescriptionPost = () => {
    dispatch(
      updateDescription({
        userid: selectedTask.userid,
        completed: false,
        task_date: selectedTask.task_date,
        task_description: taskDescription,
        task_id: selectedTask.id,
      })
    );
  };

  return (
    <SelectedTaskContainer>
      <SelectedHeader>
        <SelectedTaskTitle>
          {selectedTask ? selectedTask.task_title : null}
        </SelectedTaskTitle>
        {selectedTask && (
          <CheckedButton>
            {selectedTask.completed == true && <Check size="25" />}
            {selectedTask.completed == false && <Todo size="25" />}
            {selectedTask.completed ? "MARQUÉ COMME TERMINÉ" : "A RÉALISER"}
          </CheckedButton>
        )}
      </SelectedHeader>
      {selectedTask && (
        <>
          <Selectors />
          <TaskDescription>
            <FlexContainer>
              <PencilSquare size="25" />
              <Description>Description</Description>
            </FlexContainer>
            <TextArea
              type="text"
              key={
                selectedTask.task_description
                  ? selectedTask.task_description
                  : ""
              }
              defaultValue={
                selectedTask.task_description
                  ? selectedTask.task_description
                  : ""
              }
              onChange={(e) => handleTaskChange(e)}
            />
          </TaskDescription>
          <CommentSection>
            <CommentButton
              type="submit"
              onClick={(e) => handleDescriptionPost(e)}
            >
              COMMENTER
            </CommentButton>
          </CommentSection>
        </>
      )}
    </SelectedTaskContainer>
  );
}

export default SelectedTask;
