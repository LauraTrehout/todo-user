import { useSelector, useDispatch } from "react-redux";

import fr from "date-fns/locale/fr";
import format from "date-fns/format";

import { Check } from "@styled-icons/bootstrap/Check";
import { TodoCheckbox } from "../../styles/Checkbox.styled";
import {
  TodoItemContainer,
  TaskDetails,
  FlexContainer,
} from "../../styles/Container.styled";
import { TaskDate, TaskTitle, TaskUser } from "../../styles/Title.styled";

import {
  newSelectedTask,
  resetSelectedTask,
} from "../../redux/actions/selectedTask.actions";
import { setCompleted } from "../../redux/actions/tasks.actions";

const TodoItem = ({ task, token_id }) => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users.users);
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);
  const handleTaskClick = () => {
    dispatch(newSelectedTask(task));
  };

  const toggleTask = () => {
    if (selectedTask !== "") {
      dispatch(
        setCompleted({
          userid: selectedTask.userid,
          completed: true,
          task_date: selectedTask.task_date,
          task_description: selectedTask.task_description,
          task_id: selectedTask.id,
        })
      );
      dispatch(resetSelectedTask());
    }
  };

  const getDate = () => {
    if (task.task_date) {
      if (
        task.task_date.toString().slice(0, 10) ===
        new Date().toString().slice(0, 10)
      ) {
        return "Aujourd'hui";
      } else if (task.task_date === "") {
        return "";
      } else {
        return format(new Date(task.task_date), "dd MMMM yyyy", { locale: fr });
      }
    }
  };

  const getUser = () => {
    return users.map((user) => {
      if (task.userid == user.id) {
        return user.user_name;
      }
    });
  };

  return (
    <TodoItemContainer>
      <FlexContainer>
        <TodoCheckbox onClick={toggleTask}>
          <Check color="white" />
        </TodoCheckbox>
        <TaskDetails>
          <TaskTitle onClick={handleTaskClick}>{task.task_title}</TaskTitle>
          <TaskUser>{getUser()}</TaskUser>
        </TaskDetails>
      </FlexContainer>
      <TaskDate>{getDate()}</TaskDate>
    </TodoItemContainer>
  );
};

export default TodoItem;

