import { TasksContainer } from "../styles/Container.styled";

import Done from "./Done/Done";
import Todo from "./Todo/Todo";

function Tasks({ input, setInput}) {

  return (
    <TasksContainer>
      <Todo
        input={input}
        setInput={setInput}
      />
      <Done />
    </TasksContainer>
  );
}

export default Tasks;
