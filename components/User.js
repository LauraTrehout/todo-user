import { useDispatch, useSelector } from "react-redux";

import { updateUser } from "../redux/actions/tasks.actions";

import { UserCircle } from "@styled-icons/boxicons-regular/UserCircle";
import { UserList } from "../styles/List.styled";
import { UserEmail } from "../styles/Title.styled";
import { FlexContainer, UserDetails } from "../styles/Container.styled";

const User = ({ user, setShowUsers }) => {
  const dispatch = useDispatch();
  const selectedTask = useSelector((state) => state.selectedTask.selectedTask);
  const handleUserClick = () => {
    dispatch(updateUser({
      userid: user.id,
        completed: false,
        task_date: selectedTask.task_date,
        task_description: selectedTask.task_description,
        task_id: selectedTask.id,
    }))
    setShowUsers(false);
  };

  return (
    <UserList onClick={handleUserClick}>
      <FlexContainer>
        <UserCircle size="40" />
        <UserDetails>
          {user.user_name}
          <UserEmail>{user.user_email}</UserEmail>
        </UserDetails>
      </FlexContainer>
    </UserList>
  );
};

export default User;
