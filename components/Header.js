import Image from "next/image";

import todoist from "../public/assets/todoist.png";
import { StyledHeader } from "../styles/Header.styled";
import Logout from "./Auth/Logout";

function Header({ logout }) {
  return (
    <StyledHeader>
      <Image src={todoist} alt="logo" />
      {logout && <Logout />}
    </StyledHeader>
  );
}

export default Header;
