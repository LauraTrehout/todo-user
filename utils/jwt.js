const jwt = require('jsonwebtoken');

const createToken = ({ id, name, email }) => {
  return jwt.sign(
    { id: id, name: name, email: email },
    process.env.PRIVATE_KEY
  );
};

module.exports = { createToken };
