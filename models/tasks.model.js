const { pool } = require("../config/db");

const getAll = () => {
  let sql = `SELECT * FROM tasks`;
  return new Promise((resolve, reject) => {
    pool.query(sql, (err, result) => {
      if (err) reject(err);
      resolve(result.rows);
    });
  });
};

const getById = (task_id) => {
  let sql = `SELECT * FROM tasks WHERE id = $1`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [task_id], (err, result) => {
      if (err) reject(err);
      resolve(result.rows[0]);
    });
  });
};

const getTasksOf = (userid) => {
  let sql = `SELECT * FROM tasks WHERE userid = $1`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [userid], (err, result) => {
      if (err) reject(err);
      resolve(result.rows ? result.rows : []);
    });
  });
};

const create = ({ task_title, completed, userid }) => {
  let sql = `INSERT INTO tasks (task_title, completed, userid) VALUES ($1, $2, $3)`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [task_title, completed, userid], (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
};

const update = ({
  userid,
  completed,
  task_date,
  task_description,
  taskId,
}) => {
  let sql = `UPDATE tasks SET userid = $1, completed = $2, task_date = $3, task_description = $4 WHERE id = $5`;
  return new Promise((resolve, reject) => {
    pool.query(
      sql,
      [userid, completed, task_date, task_description, taskId],
      (err, result) => {
        if (err) reject(err);
        resolve(result);
      }
    );
  });
};

const deleteOne = (task_id) => {
  let sql = `DELETE FROM tasks WHERE id = $1`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [task_id], (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
};

module.exports = { getAll, getById, getTasksOf, create, deleteOne, update };
