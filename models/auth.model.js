const { pool } = require("../config/db");

const getByEmail = async (email) => {
  let sql = `SELECT * FROM users WHERE user_email = $1`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [email], (err, result) => {
      if (err) reject(err);
      resolve(result.rows[0]);
    });
  });
};

create = async ({ name, email, hashedPassword }) => {
  let sql = `INSERT INTO users (user_name, user_email, hashedpassword) VALUES ($1, $2, $3)`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [name, email, hashedPassword], (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
};

module.exports = { getByEmail, create };
