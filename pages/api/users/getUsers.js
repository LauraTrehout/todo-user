import { getAll } from "../../../models/users.model";

const getUsers = async (req, res) => {
  if (req.method === "GET") {
    const users = await getAll();
    try {
      res.status(200).json(users);
    } catch (error) {
      res.status(500).json({ message: "Error retrieving users" });
    }
  }
};

export default getUsers;
