import { update } from "../../../../models/tasks.model";

const updateTask = async (req, res) => {
  if (req.method === "PUT") {
    const { taskId } = req.query;
    const { userid, completed, task_date, task_description } = req.body;
    const updatedTask = await update({
      userid,
      completed,
      task_date,
      task_description,
      taskId,
    });
    try {
      res
        .status(200)
        .json({ message: "Task updated", updated: { ...req.body } });
    } catch (error) {
      res.status(500).json({ message: "Error updating task" });
    }
  }
};

export default updateTask;
