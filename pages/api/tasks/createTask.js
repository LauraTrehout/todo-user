import { create } from "../../../models/tasks.model";

const createTask = async (req, res) => {
  if (req.method === "POST") {
    const { task_title, completed, userid } = req.body;
    const task = await create({ task_title, completed, userid })
      .then(
        res.status(201).json({ message: "Task created", task: { ...req.body } })
      )
      .catch((err) => {
        console.error(err);
        res.status(500).json({ message: "Error posting task" });
      });
  }
};

export default createTask;
