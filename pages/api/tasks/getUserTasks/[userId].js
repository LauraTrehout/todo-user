import { getTasksOf } from "../../../../models/tasks.model";

const getUserTasks = async (req, res) => {
  if (req.method === "GET") {
    const { userId } = req.query;
    try {
      const tasks = await getTasksOf(userId);
      res.status(200).json(tasks);
    } catch (error) {
      console.error(error);
    }
  }
};

export default getUserTasks;
