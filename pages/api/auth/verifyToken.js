import * as jwt from "jsonwebtoken";
import { getById } from "../../../models/users.model";

const verifyToken = async (req, res) => {
  const { token } = req.body
  if (token) {
    try {
      const verified = jwt.verify(token, process.env.PRIVATE_KEY);
      const user = await getById(verified.id);
      if (user.user_email.toLowerCase() != verified.email.toLowerCase()) {
        throw new Error("Wrong token");
      }
      return res.status(200).json({ id: verified.id, name: verified.name });
    } catch (error) {
      if (error.message == "Wrong token") {
        return res.status(401).json({ message: "Token is not valid" });
      } else {
        return res.status(500).json({ message: "Error verifying token" });
      }
    }
  }
};

export default verifyToken;
