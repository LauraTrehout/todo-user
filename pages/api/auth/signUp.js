import { getByEmail } from "../../../models/auth.model";
import { create } from "../../../models/auth.model";
import { hashPassword } from "../../../utils/argon2";
import Joi from "joi";
import validateSignUp from "../../../lib/middlewares/validation";

const schema = Joi.object({
  name: Joi.string().min(2).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
});

export default validateSignUp({ body: schema }, async (req, res) => {
  if (req.method === "POST") {
    const { name, email, password } = req.body;
    const user = await getByEmail(email);
    if (user) {
      throw new Error("DUPLICATA");
    }
    try {
      const hashedPassword = await hashPassword(password);
      await create({ name, email, hashedPassword });
      return res.status(201).json({ message: "User registered" });
    } catch (error) {
      if (error.message === "DUPLICATA") {
        return res.status(401).json({ message: "User already exists" });
      } else {
        res.status(500).json({ message: "Error registering user" });
      }
    }
  }
});
