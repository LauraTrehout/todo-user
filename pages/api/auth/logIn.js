import { getByEmail } from "../../../models/auth.model";
import { verifyPassword } from "../../../utils/argon2";
import { createToken } from "../../../utils/jwt";

const logIn = async (req, res) => {
  if (req.method === "POST") {
    const { email, password } = req.body;
    try {
      const user = await getByEmail(email);
      if (!user) {
        throw new Error("NOT_FOUND");
      } else {
        const verifiedPassword = await verifyPassword(
          user.hashedpassword,
          password
        );
        if (!verifiedPassword) {
          throw new Error("WRONG_CREDENTIALS");
        } else {
          const access_token = createToken({
            id: user.id,
            name: user.user_name,
            email: email,
          });
          res
            .status(200)
            .setHeader("access_token", access_token)
            .json({ access_token: access_token });
        }
      }
    } catch (error) {
      if (error.message === "NOT_FOUND") {
        res.status(404).json({ message: "User doesn't exist" });
      } else if (error.message === "WRONG_CREDENTIALS") {
        res
          .status(401)
          .json({ message: "Email and password are not corresponding" });
      } else {
        return res.status(500).json({ message: "Error loging user" });
      }
    }
  }
};

export default logIn;
