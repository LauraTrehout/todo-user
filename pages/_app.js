import { wrapper } from "../redux/store";

import { ThemeProvider } from "styled-components";
import GlobalStyle from '../styles/global'

const theme = {
  colors: {
    main: "#DB4437",
    bg: "#F2F2F2",
    greyTxt: "#909191",
  },
};

function MyApp({ Component, pageProps }) {
  return (
  <>
    <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  );
}
export default wrapper.withRedux(MyApp);
