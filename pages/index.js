import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setToken } from "../redux/actions/users.actions";
import Header from "../components/Header";
import Todo from "../pages/todo";
import LogIn from "../components/Auth/LogIn";
import SignUp from "../components/Auth/SignUp";

import { AuthWrapper } from "../styles/Container.styled";

const Home = () => {
  const dispatch = useDispatch()
  const token = useSelector((state) => state.users.token);

  useEffect(() => {
    const getToken = localStorage.getItem('access_token')
    getToken ? dispatch(setToken(getToken)) : null
  }, [dispatch])

  return (
    <>
      {token ? (
        <>
          <Header logout={true} />
          <Todo />
        </>
      ) : (
        <>
          <Header logout={false} />
          <AuthWrapper>
            <SignUp />
            <LogIn />
          </AuthWrapper>
        </>
      )}
    </>
  );
};

export default Home;
