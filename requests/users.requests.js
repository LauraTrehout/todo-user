const axios = require("axios");

export const getUsersRequest = () => {
  return axios.get(`${process.env.NEXT_PUBLIC_SERVER}/api/users/getUsers`);
};

export const registerUserRequest = (signupInfo) => {
  return axios.post(
    `${process.env.NEXT_PUBLIC_SERVER}/api/auth/signUp`,
    signupInfo
  );
};

export const logUserRequest = (loginInfo) => {
  return axios.post(
    `${process.env.NEXT_PUBLIC_SERVER}/api/auth/logIn`,
    loginInfo
  );
};
