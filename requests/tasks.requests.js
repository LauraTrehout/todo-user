import axios from 'axios'

export const getTasksRequest = (token_id) => {
  return axios.get(
    `${process.env.NEXT_PUBLIC_SERVER}/api/tasks/getUserTasks/${token_id}`
  );
};

export const addTaskRequest = (newTask) => {
  return axios.post(`${process.env.NEXT_PUBLIC_SERVER}/api/tasks/createTask`, newTask);
};

export const updateTaskRequest = (task_id, updatedTask) => {
  return axios.put(`${process.env.NEXT_PUBLIC_SERVER}/api/tasks/updateTask/${task_id}`, updatedTask);
};


