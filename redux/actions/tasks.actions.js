export const types = {
  GET_TASKS: "tasks/GET_TASKS",
  SET_TASKS: "tasks/SET_TASKS",
  ADD_TASK: "tasks/ADD_TASK",
  UPDATE_USER: "tasks/UPDATE_USER",
  UPDATE_DATE: "tasks/UPDATE_DATE",
  SET_COMPLETED: "tasks/SET_COMPLETED",
  UPDATE_DESCRIPTION: "tasks/UPDATE_DESCRIPTION",
};

export function getTasks(payload) {
  return {
    type: types.GET_TASKS,
    payload,
  };
}
export function setTasks(payload) {
  return {
    type: types.SET_TASKS,
    payload,
  };
}

export function newTask(payload) {
  return {
    type: types.ADD_TASK,
    payload,
  };
}

export function updateUser(payload) {
  return {
    type: types.UPDATE_USER,
    payload,
  };
}

export function updateDate(payload) {
  return {
    type: types.UPDATE_DATE,
    payload,
  };
}

export function setCompleted(payload) {
  return {
    type: types.SET_COMPLETED,
    payload,
  };
}

export function updateDescription(payload) {
  return {
    type: types.UPDATE_DESCRIPTION,
    payload,
  };
}

