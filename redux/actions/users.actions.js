export const types = {
GET_USERS: "users/GET_USERS",
SET_USERS: "users/SET_USERS",
SIGNUP: "users/SIGNUP",
LOGIN: "users/LOGIN",
LOGOUT: "users/LOGOUT",
SET_TOKEN: "users/SET_TOKEN"
}

export function getUsers() {
    return {
      type: types.GET_USERS,
    }
  }
export function setUsers(payload) {
    return {
      type: types.SET_USERS,
      payload,
    }
  }
  export function signup(payload) {
    return {
      type: types.SIGNUP,
      payload
    };
  }
  export function login(payload) {
    return {
      type: types.LOGIN,
      payload
    };
  }
  
  export function setToken(payload) {
    return {
      type: types.SET_TOKEN,
      payload,
    };
  }
  
  export function logout(payload) {
    return {
      type: types.LOGOUT,
      payload,
    };
  }