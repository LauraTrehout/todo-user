import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { createWrapper } from "next-redux-wrapper";
import createSagaMiddleware from "redux-saga";
import rootReducer from "./reducers";
import rootSaga from "../sagas";

const bindMiddleware = (middleware) => {
  if (process.env.NODE_ENV !== "production") {
    const { composeWithDevTools } = require("redux-devtools-extension");
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

const sagaMiddleware = createSagaMiddleware();

const makeStore = () => {
  const store = createStore(
    rootReducer,
    bindMiddleware([thunk, sagaMiddleware])
  );
  store.SagaTask = sagaMiddleware.run(rootSaga);
  return store;
};
export const wrapper = createWrapper(makeStore);
