import { types as typesTasks } from "../actions/tasks.actions";

const initialState = {
  tasks: [],
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case typesTasks.SET_TASKS:
      let tasksData = action.payload;
      return {
        ...state,
        tasks: tasksData,
      };
    default:
      return state;
  }
}

export default reducer;
