import { combineReducers } from "redux";
import tasks from "./tasks.reducer";
import selectedTask from "./selectedTask.reducer"
import users from './users.reducer'

export default combineReducers({
  tasks,
  selectedTask,
  users
});
