import { types as typesUsers } from "../actions/users.actions";

const initialState = {
  users: [],
  token: null,
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case typesUsers.SET_USERS:
      let usersData = action.payload
      return {
        ...state, 
        users: usersData
      };
      case typesUsers.SET_TOKEN:
      let newToken = action.payload;
      return {
        ...state,
        token: newToken,
      };
    case typesUsers.LOGOUT:
      return {
        ...state,
        ...initialState,
      };
    default:
      return state;
  }
}

export default reducer