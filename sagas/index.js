import { all, fork } from "redux-saga/effects";
import { watchTasksSagas } from "./tasks.sagas";
import { watchUsersSagas } from "./users.sagas";

function* rootSaga() {
  yield all([fork(watchTasksSagas)]);
  yield all([fork(watchUsersSagas)]);
}

export default rootSaga;
