import { put, all, takeLatest, call } from "redux-saga/effects";
import { setToken, types as typesUsers } from "../redux/actions/users.actions";
import { setUsers } from "../redux/actions/users.actions";
import {
  getUsersRequest,
  registerUserRequest,
  logUserRequest,
} from "../requests/users.requests";

export function* getUsersSaga() {
  const { data } = yield call(getUsersRequest);
  yield put(setUsers(data));
}
export function* registerSaga(action) {
  const signupInfo = action.payload;
  try {
    const { data } = yield call(registerUserRequest, signupInfo);
    yield alert(data.message);
  } catch (error) {
    console.log(error);
  }
}

export function* loginSaga(action) {
  const loginInfo = action.payload;
  try {
    const { data } = yield call(logUserRequest, loginInfo);
    yield put(setToken(data.access_token));
    localStorage.setItem("access_token", data.access_token);
  } catch (error) {
    console.log(error);
  }
}

export function* watchUsersSagas() {
  yield all([takeLatest(typesUsers.GET_USERS, getUsersSaga)]);
  yield all([takeLatest(typesUsers.SIGNUP, registerSaga)]);
  yield all([takeLatest(typesUsers.LOGIN, loginSaga)]);
}
