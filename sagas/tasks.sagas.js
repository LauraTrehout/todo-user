import { put, all, takeLatest, call, select } from "redux-saga/effects";
import { types as typesTasks } from "../redux/actions/tasks.actions";
import { setTasks } from "../redux/actions/tasks.actions";
import {
  getTasksRequest,
  addTaskRequest,
  updateTaskRequest,
} from "../requests/tasks.requests";

export function* getTasksSaga(action) {
  // pouvoir récupérer un state depuis le store
  // const { token } = yield select((state) => state.users);
  const token_id = action.payload;
  const { data } = yield call(getTasksRequest, token_id);
  // récupérer el status de la réponse,  200 ou 400
  yield put(setTasks(data));
}

export function* addTaskSaga(action) {
  const newTask = action.payload;
  const { data } = yield call(addTaskRequest, newTask);
}

export function* updateTaskSaga(action) {
  const updatedTask = action.payload;
  const task_id = updatedTask.task_id;
  const { data } = yield call(
    updateTaskRequest,
    task_id,
    updatedTask
  );
}

export function* watchTasksSagas() {
  yield all([takeLatest(typesTasks.GET_TASKS, getTasksSaga)]);
  yield all([takeLatest(typesTasks.ADD_TASK, addTaskSaga)]);
  yield all([takeLatest(typesTasks.UPDATE_DESCRIPTION, updateTaskSaga)]);
  yield all([takeLatest(typesTasks.UPDATE_USER, updateTaskSaga)]);
  yield all([takeLatest(typesTasks.UPDATE_DATE, updateTaskSaga)]);
  yield all([takeLatest(typesTasks.SET_COMPLETED, updateTaskSaga)]);
}
