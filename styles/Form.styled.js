import styled from "styled-components";

export const AuthForm = styled.form`
width: 50%;
display: flex;
flex-direction: column ;
padding: 1rem;
`

export const AuthFormLabel = styled.label`
padding: 1rem;
`