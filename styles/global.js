import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
* { 
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    list-style-type: none;
}

body {
    dispaly: flex;
    height: 100vh;
    font-family: 'Roboto', sans-serif;
    background-color: #F2F2F2;
    margin: 0;
    padding: 0
}
`;

module.exports = GlobalStyle 